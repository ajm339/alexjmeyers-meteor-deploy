(function(){// if (Posts.find().count() === 0) {
  
//   Posts.insert({
//     title: 'A little about what I am doing right now',
//     url: 'http://www.daapr.com',
//     image_url: 'https://s3-us-west-2.amazonaws.com/alexjmeyers/alexjmeyers-static-original-post.png',
//     author: 'Alex Meyers',
//     date: new Date('December 31, 2013'),
//     body: [
//       "Right now I am in the midst of working with a great team of Cornellians to create a new social media platform called daapr. Our goal is to build an environment which strives to make all content social.",
//       "So, what is daapr?",
//       "<a href='http://www.daapr.com' target='blank'>daapr</a> is a new platform that is changing the way we share and discover online. the site allows users to curate the web with a simple, dynamically updating feed that makes sharing links amongst friends easier and more fun than ever. we’ve only just begun our journey to make content social, and we’d love your support along the way.",
//       "Unfortunately this website is pretty sparse at the moment. I am currently working day and night to finalize the alpha release of daapr! There are only two of us programming right now, myself and Connor Strynkowski, a very talented Comptuer Science major at Cornell. We are looking forward to the addition of William Tyler Nebel to the programming team to help us speed up progress and gain some of his great insight. Leading the business side of developing daapr is the very driven Aaron Schifrin. Along with Aaron we have Austin Gage and Brendan Miller working tirelessly on business development for daapr.",
//       "My goal is to have a very complete personal website/blog up in the next week or so. I am preparing to build this website using the power of <a href='http://rubyonrails.org/' target='_blank'>Ruby on Rails</a> with the frontend framework <a href='http://foundation.zurb.com/' target='_blank'>Foundation by Zurb</a>. It is difficult because I am trying to balance developing daapr, this website, my family and learning so many new technologies, but I promise to have an exciting website that tells a bit about who I am along with a showcase of some of the new technologies I have leanred up within the next week or so.",
//       "Thanks for stopping by!"
//       ]
//   });

  
//   Posts.insert({
//     title: 'My First Post',
//     url: 'http://www.daapr.com',
//     image_url: 'https://s3-us-west-2.amazonaws.com/alexjmeyers/alexjmeyers-first-post.jpg',
//     author: 'Alex Meyers',
//     date: new Date('January 24, 2014'),
//     body: [
//       "Hey there. So I just updated my site from a single static post to a fully function Rails blog with posts and comments working. So far that is about all it does, I will add more functionality when I have more time. I have been working hard on daapr and we are moving from alpha launch towards beta. Check it out!"
//       ]
//   });


//   Posts.insert({
//     title: 'The Constant iOS vs Android Battle',
//     url: 'http://bgr.com/2014/01/23/android-review-iphone-user/',
//     image_url: 'http://i0.wp.com/boygeniusreport.files.wordpress.com/2013/05/bgr-samsung-galaxy-s4-review-1.jpg?w=870',
//     author: 'Alex Meyers',
//     date: new Date('January 24, 2014'),
//     body: [
//       "So I am constantly reading articles about iOS vs Android, which is better, why the other is awful, and at a certain point this battle becomes hackneyed and annoying. Each has their benefits and annoyances. For one, I love the slim minimalistic form factor of the iPhone. I personally love my Moto X, but when it comes to sheer appearances, the iPhone has always been and will likely always be in a class of its own.",
//       "An article from BGR (linked in the image) in particular describes an iPhone user who used a Samsung Galaxy S4 for 6 months and described his grievances about both phones. He mentions that the app quality on iOS is in general a step up from that of Android, and although I am an Android fanboy, I do have to agree with the user on this one. I would not necessarily attest this difference to some notion that iOS is a better platform, but more so that although Android devices are way more plentiful worldwide to iOS devices, it still remains that developers program for iOS first and Android second. With this kind of mentality, of course the iOS apps will be better, they are the primary focus while Android is simply made available to reach a wider audience. With this kind of focus, all apps designed in this matter are optimized for iOS and are just made to work on Android.",
//       "If developers began to develop for Android before iOS, this pattern could very well flip sides. For example, apps made by Google like Google Music, Google Maps, Gmail, Google Now, Chrome and even some non-Google apps like Dolphin Browser are beautifully made and optimized for Android, because they were designed FOR Android, NOT iOS.",
//       "I particularly want to note Google Now integration on Android, specifically the Moto X. I for one have never been a fan of voice commands, but with the Active Notifications and easy-to-use interface of Google Now on my device, I am now constantly using voice commands. Voice commands seemed so annoying and a waste of time when they first became available with flip phones like the Motorola Razr (so long ago...), but this was because one would have to repeat the command multiple times and even then not find what he/she was looking for. With my Moto X, my phone can be sitting, untouched, and I can call my friends, or navigate to any destination with only my voice. Google Now was developed for Android, and it works perfectly with Android. I have seen it on iOS, the functionality of Google Now is still the same, but the ease-of-use is not.",
//       "So this brings me back to my original point, are iOS apps actually much nicer than Android versions, or is it the fact that developers are not targeting Android as a primary ecosystem? I believe that it is the former of the two. There is a part of me that understands the need to develop for iOS first, because the common belief is that the iPhone and iOS is the gold standard; additionally there is much less fragmentation for iOS than there is for Android. On the other hand, as seen in this article from <a href='http://www.engadget.com/2013/10/31/strategy-analytics-q3-2013-phone-share/' target='_blank'>Engadget</a>, Android destroys iOS in market share...so why are developers not targeting their primary efforts towards Android? So it goes..."
//       ]
//   });


//   Posts.insert({
//     title: 'My first open source contribution',
//     url: 'https://github.com/ajm339/pills',
//     image_url: 'https://s3.amazonaws.com/daapr_development_bucket/pills.png',
//     author: 'Alex Meyers',
//     date: new Date('March 10, 2014'),
//     body: [
//       "So it has been awhile since I have posted on here. Between school, daapr, and interviews it has been quite hectic. Daapr is an exciting experience with all sorts of experiences. Lately I have transitioned from a role of primarily developing to one where I must balance development and management. The new team members are wonderful; however I truly underestimated the amount of time and energy management takes, between teaching new team members, setting up their laptops with daapr code, and managing progress, I am quite exhausted!",
//       "Today I am posting because I am excited about one of the newest features on daapr, tagging posts with interests (topics). Aaron and I envisioned having little pills as tags for daapr, and so I searched the web for a good open source project that used these pills. To my dismay I could not find one. At first, Anton and I tried to just go a different direction using an existing design we had on the site, but it did not really fit well on the modal where a user confirms his/her post. With this, I set out to build the design I had envisioned. The team and I feel that it works nicely, so I decided to isolate the code and make it a public plugin on Github.",
//       "This is my first contribution to the open source community. Although the plugin is very basic and trivial, I hope that someone finds it helpful and uses it either in its current state or as inspiration for his/her own modifications."
//       ]
//   });


//   Posts.insert({
//     title: 'Showcasing daapr\'s ability to share on Facebook',
//     url: 'http://bgr.com/2014/01/23/android-review-iphone-user/',
//     image_url: 'https://s3.amazonaws.com/daapr_profile_pictures/daaprfacebook.png',
//     author: 'Alex Meyers',
//     date: new Date('March 22, 2014'),
//     body: [
//       "This blog post is a showcase of the newest features coming soon to daapr. Logging in with Facebook, discovering Facebook friends on daapr, and posting on both Facebook and daapr simultaneously. Enjoy."
//       ]
//   });


//   Posts.insert({
//     title: 'Music of the Ages',
//     url: 'http://peterblan.co/project2/project2.html',
//     image_url: 'http://www.jolicharts.com/wp-content/uploads/2013/08/D3JS.png',
//     author: 'Alex Meyers',
//     date: new Date('May 19, 2014'),
//     body: [
//       "This past semester I took INFO 3300 (Data-Driven Web Applications) at Cornell University in which we learned how to use the library d3.js and applied a variety of concepts including Poisson distribution, preferential graphs, and various others to our d3.js projects. Attached is a one of the projects that I worked with Demitri Tzitzon and Peter Blanco to create.",
//       "The graph to the side is a representation of top Billboard Songs in the last 24 years. *Note* There was too much data to represent every song in the last 24 years, so we limited it to randomly include every tenth song.",
//       "Overall it was very exciting and fun to build. I will definitely apply concepts from this class to my contributions to daapr and other projects."
//       ]
//   });


// Posts.insert({
//     title: 'Amazon Web Services Guide',
//     url: 'http://www.alexjmeyers.com/',
//     image_url: 'https://upload.wikimedia.org/wikipedia/commons/thumb/1/1d/AmazonWebservices_Logo.svg/1280px-AmazonWebservices_Logo.svg.png',
//     author: 'Alex Meyers',
//     date: new Date('August 24, 2014'),
//     body: [
//       "This summer I spent 2 long months working to learn how to migrate my Rails apps off of Heroku onto AWS. I also wanted to not just host an app on an EC2 instance, but rather create a multi-layer system that could utilize automation for adding new application layer instances. I played with many different automation tools including Chef, Puppet and AWS Opsworks in order to test many different options available. I eventually settled with AWS Opsworks because it utilized Chef's cookbook technology but incorporated a visual environment for configuring and booting a system. I received great guidance from Parker Moore at VSCO and Greg Scallan at Flipboard. Without their helpful notes, I would have been lost in my endeavor. The photo links to a page on my site dedicated to hosting the helpful guide I created to help any newbie harness the power of AWS with a simple Rails application. Enjoy."
//       ]
//   });
// };

})();
