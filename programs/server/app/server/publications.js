(function(){Meteor.publish('posts', function() {
  return Posts.find( {}, {sort: {date:-1} });
});

Meteor.publish('singlePost', function(id) {
  check(id, String);
  return Posts.find(id);
});

})();
