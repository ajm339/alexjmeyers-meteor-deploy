(function(){Router.configure({
  layoutTemplate: 'layout',
  loadingTemplate: 'loading',
  notFoundTemplate: 'notFound',
});

Router.route('/', {
	name: 'index'
});

Router.route('/about', {
  name: 'about'
});

Router.route('/projects', {
  name: 'projects'
});

Router.route('/blog', {
	name: 'blog',
	waitOn: function() { 
		return Meteor.subscribe('posts'); 
	}
});

Router.route('/blog/:_id', {
  name: 'postPage',
  waitOn: function() {
    return Meteor.subscribe('singlePost', this.params._id);
  },
  data: function() { return Posts.findOne(this.params._id); }
});

Router.route('/contact', {
  name: 'contact'
});

})();
