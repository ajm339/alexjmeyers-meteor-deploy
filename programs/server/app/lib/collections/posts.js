(function(){Posts = new Mongo.Collection('posts');

/***Documentation


splitString()
*Arguements take JSON object with keys "stringToSplit", "separator", "numWordsToReturn"
*Returns an array of Strings


***/

Meteor.methods({
	splitString: function(input) {
		var stringToSplit = input["stringToSplit"].toString();
		var separator = input["separator"];
		var numWordsToReturn = parseInt(input["numWordsToReturn"]);

		var arrayOfStrings = stringToSplit.split(separator);
		return arrayOfStrings.slice(0, numWordsToReturn);
	}
});

})();
